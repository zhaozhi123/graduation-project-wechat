Component({
  data: {
    rippleVisible: false
  },
  methods: {
    handleLongPress() {
      this.setData({
        rippleVisible: true
      });
    },
    handleTouchEnd() {
      this.setData({
        rippleVisible: false
      });
    }
  }
});