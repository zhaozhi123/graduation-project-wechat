Page({
  data: {
    htmlSnip: '',
  },
  onLoad: function (options) {
    wx.request({
      url: 'http://127.0.0.1:8081/article/getById',
      data: { _id: options._id },
      header: { 'content-type': 'application/json' },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result) => {
        console.log(result)
        this.setData({ htmlSnip: result.data[0].context })
      },
      fail: () => { console.log("err", 1111111111) },
    })
  },
})

