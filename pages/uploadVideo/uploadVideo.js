let timer
Page({
  data: {
    isSafe: "",
    isShow: false,
    isLodding: false,
    rippleVisible: false,
    tempFilePath: "",
    filename: "现录制语音无文件名",
    baidu: {},
    text:'',
  },
  //提交
  submit() {
    let that = this;
    if (that.data.tempFilePath) {
      this.setData({ isLodding: true })
      wx.uploadFile({
        url: 'http://127.0.0.1:5000/predict1',
        filePath: that.data.tempFilePath,   //文件路径
        name: 'file',
        success: function (res) {

          let { result, baiduResult, text } = JSON.parse(res.data);
          let decodedString_text = unescape(text.replace(/\\u/g, "%u")); // 解码 Unicode 转义序列
          let decodedString_baiduResult = unescape(baiduResult.replace(/\\u/g, "%u")); // 解码 Unicode 转义序列
          text = unescape(decodedString_text); // 解码转义序列
          baiduResult = unescape(decodedString_baiduResult); // 解码转义序列

          console.log(baiduResult);
          var parts = baiduResult.split("&");
          // 创建一个空对象
          var obj = {};
          // 遍历分割后的部分
          for (var i = 0; i < parts.length; i++) {
            // 通过 ":" 分割每个部分
            var keyValue = parts[i].split("：");
            // 获取属性名和属性值
            var key = keyValue[0];
            var value = keyValue[1];
            // 将属性名和属性值添加到对象中
            obj[key] = value;
          }
          that.setData({ isSafe: result, isShow: true, isLodding: false, baidu: {'answer':obj.answer,'why':obj.why},text })
          wx.request({
            url: 'http://127.0.0.1:8081/historyList/post',
            data: {
              'user': wx.getStorageSync('username'),
              'issafe': result == 'safe' ? '安全' : '不安全',
              'context': text,
              'date': that.getTime()
            },
            header: { 'content-type': 'application/json' },
            method: 'GET',
            dataType: 'json',
            responseType: 'text',
            success: (result) => {

            },
            fail: () => { console.log("err", 1111111111) },
          })
          // }
        },
        fail: function (err) {
          console.log(err)
          that.setData({ isLodding: false })
        }
      })
    } else {
      wx.showToast({
        title: '暂无可提交录音',
        icon: 'error'
      })
    }
  },
  //开始录音
  start() {
    const that = this
    timer = setInterval(() => {      //开启动画，并且循环显示
      this.setData({
        rippleVisible: !that.data.rippleVisible
      });
    }, 500);
    this.recorderManager.start({
      duration: 60000,
      sampleRate: 16000, //采样率，有效值 8000/16000/44100
      numberOfChannels: 1, //录音通道数，有效值 1/2
      encodeBitRate: 32000, //编码码率
      format: 'wav', //音频格式，有效值 aac/mp3
      // frameSize: 50, //指定帧大小
      audioSource: 'auto' //指定录音的音频输入源，可通过 wx.getAvailableAudioSources() 获取
    })
  },
  //录音停止
  stop() {
    this.recorderManager.stop()
    clearInterval(timer); //清除定时器
    this.setData({        //关闭动画
      rippleVisible: false
    });
  },
  //播放录音
  play() {
    // 获取innerAudioContext实例
    const innerAudioContext = wx.createInnerAudioContext()
    // 是否自动播放
    innerAudioContext.autoplay = true
    // 设置音频文件的路径
    innerAudioContext.src = this.data.tempFilePath;
    // 播放音频文件
    innerAudioContext.onPlay(() => {
      console.log('开始播放')
    });
    // 监听音频播放错误事件
    innerAudioContext.onError((res) => {
      console.log(res.errMsg)
      console.log(res.errCode)
    })
  },
  //生命周期函数--监听页面加载
  onLoad(options) {
    var that = this;
    //获取全局唯一的录音管理器 RecorderManager实例
    that.recorderManager = wx.getRecorderManager()
    that.recorderManager.onStop((res) => {
      that.setData({
        tempFilePath: res.tempFilePath, // 文件临时路径
        filepath: res.tempFilePath,
      })
      console.log('获取到文件：' + that.data.tempFilePath)
    })
    this.recorderManager.onError((res) => {
      console.log('录音失败了！')
      //console.log(res)
    })
  },
  //获取当前时间
  getTime() {
    // 创建一个新的 Date 对象
    var currentDate = new Date();

    // 获取当前时间的小时、分钟和秒
    var hours = currentDate.getHours();
    var minutes = currentDate.getMinutes();
    var seconds = currentDate.getSeconds();

    // 获取当前时间的年、月、日
    var year = currentDate.getFullYear();
    var month = currentDate.getMonth() + 1; // 月份从 0 开始，所以需要加 1
    var day = currentDate.getDate();

    // 输出当前时间
    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds

  }
})

