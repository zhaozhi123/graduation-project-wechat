// index.js
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'

Component({
  data: {
    array: [{
      message: 'foo',
    }, {
      message: 'bar'
    }],
    motto: 'Hello World',
    userInfo: {
      avatarUrl: defaultAvatarUrl,
      nickName: '',
    },
    hasUserInfo: false,
    canIUseGetUserProfile: wx.canIUse('getUserProfile'),
    canIUseNicknameComp: wx.canIUse('input.type.nickname'),
  },
  methods: {
    //获取用户名和密码
    formSubmit(e) {
      const { username, password, passwordAgain } = e.detail.value
      if (password != passwordAgain) {
        wx.showToast({
          title: '两次密码不一致',
          icon: 'none',
          duration: 1000,
        })
      }
      else if ((username != '') && (password != '') && (passwordAgain != '')) {
        wx.request({
          url: 'http://127.0.0.1:8081/accountManage/register',
          data: { username, password },
          header: { 'content-type': 'application/json' },
          method: 'get',
          dataType: 'json',
          responseType: 'text',
          success: (result) => {
            if (result.data.ok === 1) {
              wx.showToast({
                title: '注册成功',
                icon: 'success',
                duration: 1000
              })
              setTimeout(() => {
                wx.redirectTo({
                  url: '/pages/index/index',
                })
              }, 1000);
            } else {
              wx.showToast({
                title: '该账号已经存在',
                icon: 'error',
                duration: 1000
              })
            }
          },
          fail: () => { console.log("err", 1111111111) },
          complete: () => { }
        });
      } else {
        wx.showToast({
          title: '请输入账号密码',
          icon: 'error',
          duration: 1000 //持续的时间
        })
      }

    },
    //注册账户
    onRegister() {
      wx.redirectTo({
        url: '/pages/logs/logs',
      })
    },

    // 事件处理函数
    bindViewTap() {
      wx.redirectTo({
        url: '../logs/logs'
      })
    },
    onChooseAvatar(e) {
      const { avatarUrl } = e.detail
      const { nickName } = this.data.userInfo
      this.setData({
        "userInfo.avatarUrl": avatarUrl,
        hasUserInfo: nickName && avatarUrl && avatarUrl !== defaultAvatarUrl,
      })
    },
    onInputChange(e) {
      const nickName = e.detail.value
      const { avatarUrl } = this.data.userInfo
      this.setData({
        "userInfo.nickName": nickName,
        hasUserInfo: nickName && avatarUrl && avatarUrl !== defaultAvatarUrl,
      })
    },
    getUserProfile(e) {
      // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
      wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          console.log(res)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    },
  },
})
