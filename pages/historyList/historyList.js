// pages/historyList/historyList.js
Page({
  data: {
    historyData:[],
  },
  onLoad(options) {
    wx.request({
      url: 'http://127.0.0.1:8081/historyList/get',
      data: { 'user': wx.getStorageSync('username') },
      header: { 'content-type': 'application/json' },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result) => {
        let resdate = result.data
        this.setData({historyData:resdate})
      },
      fail: () => { console.log("err", 1111111111) },
    })
  },
})