//index.js
Page({
  data: {
    //轮播图配置
    autoplay: true,
    interval: 3000,
    duration: 1200,
    navList: [{
      'src': '../../icons/yuyin2.png',
      'title': '录制语音',
      'url': 'pages/uploadVideo/uploadVideo',
    },
    {
      'src': '../../icons/yuyin2.png',
      'title': '提交音频文件',
      'url': 'pages/uploadVideoFile/uploadVideoFile',
    },
    // {
    //   'src': '../../icons/wenben.png',
    //   'title': '提交文本',
    //   "url": 'pages/submitMessage/submitMessage',
    // },
    {
      'src': '../../icons/lishixiaoxi.png',
      'title': '历史记录',
      "url": 'pages/historyList/historyList',
    },
    {
      'src': '../../icons/fankuixinxi.png',
      'title': '我要反馈'
    },
    ],
    tabs: ['欺诈预防', '金融安全','老年人防骗','学生贷款欺诈预防'],
  },
  onLoad: function () {
    var that = this;
    //初始化轮播图
    var data = {
      "datas": [
        {
          "id": 1,
          "imgurl": "../../icons/1.jpg"
        },
        {
          "id": 2,
          "imgurl": "../../icons/2.jpg"
        },
        {
          "id": 3,
          "imgurl": "../../icons/3.jpg"
        },
        {
          "id": 4,
          "imgurl": "../../icons/4.jpg"
        }
      ]
    };
    that.setData({
      lunboData: data.datas,
    });
    //加载文章
    wx.request({
      url: 'http://127.0.0.1:8081/article/get',
      data: {category:'欺诈预防'},
      header: { 'content-type': 'application/json' },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: (result) => {
        if (result.data == 0) {
        } else {
          this.setData({ dataList: result.data })
        }
      },
      fail() {
        wx.showToast({
          title: 'node后台未启动',
          icon: 'error'
        })
      },
    })
  },
  // 主页点击菜单跳转
  navClick(e) {
    let url = '/' + e.currentTarget.dataset.item.url;
    if (e.currentTarget.dataset.item.url) {
      wx.redirectTo({
        url,
      })
    }
  },
  // 主页点击文章进行跳转
  detailClick(e) {
    let article = e.currentTarget.dataset.item;
    // console.log(article._id)
    wx.navigateTo({
      url: '/pages/showArticle/showArticle?_id=' + article._id,
    })
  },
  //改变文章
  changeArticle(e){
  //加载文章
  wx.request({
    url: 'http://127.0.0.1:8081/article/get',
    data: {category:e.detail.name},
    header: { 'content-type': 'application/json' },
    method: 'GET',
    dataType: 'json',
    responseType: 'text',
    success: (result) => {
      // console.log(result.data)
      if (result.data == 0) {
        this.setData({ dataList: [] })
      } else {
        this.setData({ dataList: result.data })
      }
    },
    fail() {
      wx.showToast({
        title: 'node后台未启动',
        icon: 'error'
      })
    },
  })
  }
})
