Page({
  data: {
    username: "游客模式,点击登录",
    isLogin: false
  },
  checkIsLogin() {
    if (wx.getStorageSync('username')) {
      wx.showToast({
        title: '已登录',
        icon: 'none',
        duration: 1000,
      })
    } else {
      wx.redirectTo({
        url: '/pages/index/index',
      })
    }
  },

  exitLogin() {
    let that = this;
    if (wx.getStorageSync('username')) {
      wx.showModal({
        title: '提示',
        content: '确定退出吗？',
        success: function (res) {
          if (res.confirm) {
            wx.removeStorageSync('username');
            that.setData({ username: '未登录', isLogin: false });  //退出登录以后改变用户状态
          } else {
            // console.log('用户点击取消') 
          }
        }
      })
    } else {
      wx.showToast({
        title: '用户未登录',
        icon: 'none',
        duration: 1000
      })
    }
  },
  changePassword() {
    if (wx.getStorageSync('username')) {
      wx.showModal({
        title: '请输入原密码',
        editable: true,
        success(res) {
          if (res.confirm) {
            wx.request({
              url: 'http://127.0.0.1:8081/accountManage/login',
              data: { username: wx.getStorageSync('username'), password: res.content },
              header: { 'content-type': 'application/json' },
              method: 'get',
              dataType: 'json',
              responseType: 'text',
              success: (result) => {
                if (result.data.ok === 1) {
                  wx.showModal({
                    title: '请输入新密码',
                    editable: true,
                    success(res) {
                      if (res.confirm) {
                        wx.request({
                          url: 'http://127.0.0.1:8081/accountManage/changepassword',
                          data: { username: wx.getStorageSync('username'), password: res.content },
                          header: { 'content-type': 'application/json' },
                          method: 'get',
                          dataType: 'json',
                          responseType: 'text',
                          success: (result) => {
                            if (result.data.ok === 1) {
                              wx.showToast({
                                title: '修改成功',
                                icon: 'success',
                                duration: 1000
                              })
                            } else {
                              wx.showToast({
                                title: '修改失败',
                                icon: 'error',
                                duration: 1000
                              })
                            }
                          }
                        })
                      }
                      else if (res.cancel) {
                        console.log('用户点击取消2')
                      }
                    }
                  })
                } else {
                  wx.showToast({
                    title: '密码错误',
                    icon: 'none',
                    duration: 1000
                  })
                }
              },
            })
          } else if (res.cancel) {
            console.log('用户点击取消1')
          }
        }
      })
    } else {
      wx.showToast({
        title: '用户未登录',
        icon: 'none',
        duration: 1000
      })
    }
  },
  onLoad(options) {
    if (wx.getStorageSync('username')) {
      this.setData({
        username: wx.getStorageSync('username'),
        isLogin: true
      })
    }
  },  
  goTo(){
    console.log(1);
wx.redirectTo({
  url: '/pages/historyList/historyList',
})
  },
})