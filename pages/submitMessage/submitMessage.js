// pages/submitVoice/submitVoice.js
Page({
  //初始化数据
  data: {
    editorValue: '',
  },
  //设置value值
  textareaChang(e) {
    this.setData({ editorValue: e.detail.text });
  },

  //点击提交之后的逻辑
  submitValue() {
    let Info = this.data.editorValue;    //命名为Info是与后台协商出的字段
    if (Info.length > 1) {
      wx.request({
        url: 'http://127.0.0.1:5000/predict',
        data: { Info },
        header: { 'content-type': 'application/json' },
        method: 'POST',
        dataType: 'json',
        responseType: 'text',
        success(res) {
          console.log(res.data.result);
        },
      })
    } else {
      wx.showToast({
        title: '请输入短信',
        icon: "error"
      })
    }

  },
})