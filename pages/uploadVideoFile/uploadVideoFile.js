// pages/uploadVideoFile/uploadVideoFile.js
Page({
  data: {
    filepath:"",
    filename:"",
    isSafe:"",
    isShow:false,
    isLodding:false,
    baidu: {},
    text:'',
  },
  //上传录音文件
  chooseFile () {
    let that = this;
    wx.chooseMessageFile({
      type:'file',
      success: function (res) {
        that.setData({filepath:res.tempFiles[0].path,filename:res.tempFiles[0].name})
      }
    })
  },
  //发送文件至后台
  uploadFile(){
    let that = this
    if(that.data.filepath!=""){
      this.setData({isLodding:true})
      wx.uploadFile({
        url: 'http://127.0.0.1:5000/predict1',
        filePath: that.data.filepath,    // 本地文件路径，即选择文件返回的路径
        name: 'file',                    //上传文件的key，后台要用到
        success: function (res) {

          let { result, baiduResult, text } = JSON.parse(res.data);
          let decodedString_text = unescape(text.replace(/\\u/g, "%u")); // 解码 Unicode 转义序列
          let decodedString_baiduResult = unescape(baiduResult.replace(/\\u/g, "%u")); // 解码 Unicode 转义序列
          text = unescape(decodedString_text); // 解码转义序列
          baiduResult = unescape(decodedString_baiduResult); // 解码转义序列

          console.log(baiduResult);
          var parts = baiduResult.split("&");
          // 创建一个空对象
          var obj = {};
          // 遍历分割后的部分
          for (var i = 0; i < parts.length; i++) {
            // 通过 ":" 分割每个部分
            var keyValue = parts[i].split("：");
            // 获取属性名和属性值
            var key = keyValue[0];
            var value = keyValue[1];
            // 将属性名和属性值添加到对象中
            obj[key] = value;
          }
          that.setData({ isSafe: result, isShow: true, isLodding: false, baidu: {'answer':obj.answer,'why':obj.why},text })
          wx.request({
            url: 'http://127.0.0.1:8081/historyList/post',
            data: { 'user': wx.getStorageSync('username') ,
            'issafe':result == 'safe'?'安全':'不安全',
            'context':text,
            'date':that.getTime() },
            header: { 'content-type': 'application/json' },
            method: 'GET',
            dataType: 'json',
            responseType: 'text',
            success: (result) => {
            },
            fail: (err) => { console.log(err) },
          })

        },
        fail:function(err){
          that.setData({isLodding:false})
          wx.showToast({
            title: '出现错误',
            icon:'error'
          })
        }
      })
    }
    else{
      wx.showToast({
        title: "请选择上传文件",
        icon:'error',
      })
    }
  },
  //清除文件，防止重复提交
  clearFile(){
    this.setData({filepath:"",filename:""})
  },
  //获取当前时间
  getTime(){
    // 创建一个新的 Date 对象
var currentDate = new Date();

// 获取当前时间的小时、分钟和秒
var hours = currentDate.getHours();
var minutes = currentDate.getMinutes();
var seconds = currentDate.getSeconds();

// 获取当前时间的年、月、日
var year = currentDate.getFullYear();
var month = currentDate.getMonth() + 1; // 月份从 0 开始，所以需要加 1
var day = currentDate.getDate();

// 输出当前时间
return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds

  }
})